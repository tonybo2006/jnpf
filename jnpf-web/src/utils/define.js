// 开发环境接口配置
// const APIURl = 'http://192.168.5.101:30000'
// const WEB_URL='http://192.168.5.101'

const APIURl = "http://192.168.5.86:30000";
const WEB_URL = "http://192.168.5.86";
module.exports = {
  APIURl: APIURl,
  timeout: process.env.NODE_ENV === "development" ? 10000 : 1000000,
  WebSocketUrl:
    process.env.NODE_ENV === "development"
      ? APIURl.replace("http", "ws") + "/api/system/Message/websocket"
      : process.env.VUE_APP_BASE_WSS,
  comUploadUrl: process.env.VUE_APP_BASE_API + "/api/file/Uploader",
  comUrl: process.env.VUE_APP_BASE_API,
  // 大屏应用前端路径
  dataV:
    process.env.NODE_ENV === "development"
      ? "http://localhost:8100/DataV"
      : WEB_URL + "/DataV",
  // 数据报表
  reportServer:
    process.env.NODE_ENV === "development"
      ? "http://localhost:30000"
      : process.env.VUE_APP_BASE_API + "/",
  report:
    process.env.NODE_ENV === "development"
      ? "http://localhost:8200"
      : WEB_URL + "/Report",
  version: "1.0.2",
};
